import React from "react";
import MonacoEditor from "./monaco-editor";

export default class Editor extends React.Component {
  public render() {
    return (
      <MonacoEditor
        options={{
          minimap: {
            enabled: false
          },
          lineNumbers: "off",
          scrollbar: {
            vertical: "hidden",
            horizontal: "hidden",
            verticalScrollbarSize: 2,
            useShadows: false
          },
          overviewRulerBorder: false,
          hideCursorInOverviewRuler: true,
          renderLineHighlight: "none",
          highlightActiveIndentGuide: false,
          selectionHighlight: false,
          wordWrap: "bounded",
          scrollBeyondLastLine: false,
          renderIndentGuides: false
        }}
      />
    );
  }
}
