/**
 * From https://github.com/superRaytin/react-monaco-editor
 * modified by Cinwell(@qingwei-li)
 */
import * as monaco from "monaco-editor/esm/vs/editor/editor.api";
import React from "react";

function noop() {
  return;
}

interface IProps {
  width: string;
  height: string;
  value: string | null;
  defaultValue: string;
  language: string;
  theme: string | null;
  options: monaco.editor.IEditorConstructionOptions;
  editorDidMount: (editor, monaco?) => any;
  editorWillMount: (editor, monaco?) => any;
  onChange: (editor, event) => void;
}

export default class MonacoEditor extends React.Component<IProps> {
  public static defaultProps: IProps = {
    width: "100%",
    height: "100%",
    value: null,
    defaultValue: "",
    language: "markdown",
    theme: null,
    options: {},
    editorDidMount: noop,
    editorWillMount: noop,
    onChange: noop
  };

  public containerElement: HTMLElement | undefined;

  public currentValue;

  public preventTriggerChangeEvent;

  public editor: monaco.editor.IStandaloneCodeEditor | undefined;

  constructor(props) {
    super(props);
    this.currentValue = props.value;
  }

  public componentDidMount() {
    this.initMonaco();
  }

  public componentDidUpdate(prevProps: IProps) {
    if (this.props.value !== this.currentValue) {
      // Always refer to the latest value
      this.currentValue = this.props.value;
      // Consider the situation of rendering 1+ times before the editor mounted
      if (this.editor) {
        this.preventTriggerChangeEvent = true;
        this.editor.setValue(this.currentValue);
        this.preventTriggerChangeEvent = false;
      }
    }
    if (prevProps.language !== this.props.language) {
      if (this.editor) {
        monaco.editor.setModelLanguage(
          this.editor.getModel(),
          this.props.language
        );
      }
    }
    if (prevProps.theme !== this.props.theme && this.props.theme) {
      monaco.editor.setTheme(this.props.theme);
    }
    if (
      this.editor &&
      (this.props.width !== prevProps.width ||
        this.props.height !== prevProps.height)
    ) {
      this.editor.layout();
    }
  }

  public componentWillUnmount() {
    this.destroyMonaco();
  }

  public editorWillMount() {
    const { editorWillMount } = this.props;
    const options = editorWillMount(monaco);
    return options || {};
  }

  public editorDidMount(editor) {
    this.props.editorDidMount(editor, monaco);
    editor.onDidChangeModelContent(event => {
      const value = editor.getValue();

      // Always refer to the latest value
      this.currentValue = value;

      // Only invoking when user input changed
      if (!this.preventTriggerChangeEvent) {
        this.props.onChange(value, event);
      }
    });
  }

  public initMonaco() {
    const value =
      this.props.value !== null ? this.props.value : this.props.defaultValue;
    const { language, theme, options } = this.props;
    if (this.containerElement) {
      // Before initializing monaco editor
      Object.assign(options, this.editorWillMount());
      this.editor = monaco.editor.create(this.containerElement, {
        value,
        language,
        ...options
      });

      if (theme) {
        monaco.editor.setTheme(theme);
      }
      // After initializing monaco editor
      this.editorDidMount(this.editor);
    }
  }

  public destroyMonaco() {
    if (typeof this.editor !== "undefined") {
      this.editor.dispose();
    }
  }

  public assignRef = component => {
    this.containerElement = component;
  };

  public render() {
    const { width, height } = this.props;
    const style = {
      width,
      height
    };

    return (
      <div
        ref={this.assignRef}
        style={style}
        className="react-monaco-editor-container"
      />
    );
  }
}
