import React from "react";
import Editor from "./components/editor";

export default class App extends React.Component {
  public render() {
    return (
      <>
        <header className="app-header">title</header>
        <main>
          <Editor />
        </main>
      </>
    );
  }
}
