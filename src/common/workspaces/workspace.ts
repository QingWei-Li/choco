export abstract class Workspace {
  public abstract fetch(): void;
  public abstract add(): void;
  public abstract remove(): void;
  public abstract update(): void;
}
