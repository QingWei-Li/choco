const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const MonacoWebpackPlugin = require("monaco-editor-webpack-plugin");

const miniLoaders = [MiniCssExtractPlugin.loader, "css-loader"];
const cssHotLoader =
  process.env.NODE_ENV === "production"
    ? miniLoaders
    : ["css-hot-loader"].concat(miniLoaders);

module.exports = {
  module: {
    rules: [
      {
        test: /\.styl$/,
        use: cssHotLoader.concat("stylus-loader")
      }
    ]
  },
  plugins: [
    new MonacoWebpackPlugin({
      features: [
        "accessibilityHelp",
        "bracketMatching",
        "caretOperations",
        "clipboard",
        "codeAction",
        // "codelens",
        "colorDetector",
        "comment",
        "contextmenu",
        "coreCommands",
        "cursorUndo",
        "dnd",
        "find",
        "folding",
        "fontZoom",
        "format",
        "goToDefinitionCommands",
        "goToDefinitionMouse",
        // "gotoError",
        "gotoLine",
        "hover",
        "inPlaceReplace",
        "inspectTokens",
        // "iPadShowKeyboard",
        "linesOperations",
        "links",
        "multicursor",
        "parameterHints",
        "quickCommand",
        "quickOutline",
        "referenceSearch",
        // "rename",
        // "smartSelect",
        // "snippets",
        // "suggest",
        "toggleHighContrast",
        "toggleTabFocusMode",
        "transpose"
        // "wordHighlighter",
        // "wordOperations",
        // "wordPartOperations"
      ]
    })
  ]
};
